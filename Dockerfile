FROM python:3.10.2-slim-buster

ENV PYTHONONBUFFERED=1
ENV PYTHONDONTWRITEBYTECODE=1

WORKDIR /code


COPY Pipfile Pipfile.lock /code/

RUN python3 -m pip install pipenv \
 && python3 -m pipenv install --system --deploy


COPY . /code/
