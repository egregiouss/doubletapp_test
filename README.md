# Template for backend course
## DEPLOY
deployed in  ```http://51.250.30.74:8000/```
sdf
path to application directory on server  ```/test/projects/doubletapp_test ```
## RUN
run: ```make dev ```

migrate: ```make migrate ```

create superuser: ```make createsuperuser ```

## REST API
to get info about profile:
```api/me?tg_name=tg_username?token=your_auth_token```

## bot commands
bot name: @doubleTestBankBot
#### commands:
* /start
* /set_phone
* /me
* /card_balance
* /account_balance
* /add_friend
* /remove_friend
* /show_friends
* /transfer_money_to_account <from card> <to account> <amount>
* /transfer_money_to_card <from card> <to card> <amount>
* /transfer_money_by_username <to username>
* print

#### Done
* nginx
* webhooks
* all in .env
* update models
* decorators
* add to friend list
* remove from friend list
