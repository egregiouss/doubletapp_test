from django.contrib import admin

from app.internal.models.card_record import CardRecord


@admin.register(CardRecord)
class CardRecordAdmin(admin.ModelAdmin):
    list_display = ("from_user", "card_from", "to_user", "card_to", "amount")
