from django.contrib import admin

from app.internal.models.bank_account import BankAccount


@admin.register(BankAccount)
class AccountAdmin(admin.ModelAdmin):
    list_display = ("id", "balance")
