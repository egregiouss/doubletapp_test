from django.contrib import admin

from app.internal.models.account_record import AccountRecord


@admin.register(AccountRecord)
class AccountRecordAdmin(admin.ModelAdmin):
    list_display = ("from_user", "acc_from", "to_user", "acc_to", "amount")
