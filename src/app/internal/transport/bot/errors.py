class BotErrors:
    @staticmethod
    def no_friend_message(friend):
        return f"no friend with name ${friend} in your friendlist"

    @staticmethod
    def no_user_message():
        return "No information about you, enter /start to registrate"

    @staticmethod
    def incorrect_phone_message():
        return "Incorrect phone numer, enter /set_phone <your phone number>"

    @staticmethod
    def incorrect_friend_message():
        return "Incorrect friend username enter /add_user <friend's username>"

    @staticmethod
    def no_phone_number():
        return f"no phone number, enter {_formats['/set_phone']} to set up phone number"

    @staticmethod
    def no_card_message():
        return "No card, with entered number"

    @staticmethod
    def no_account_message():
        return "Account not found for your profile"

    @staticmethod
    def no_money_message():
        return "not enough funds :("

    @staticmethod
    def incorrect_format_message(format: str):
        return f"Incorrect command format, enter {_formats[format]}"


_formats = {
    '/transfer_money_to_account': '/transfer_money_to_account <from> <to> <amount>',
    '/transfer_money_to_card': '/transfer_money_to_card <from> <to> <amount>',
    '/set_phone': '/set_phone <your phone number>',
    '/add_user': '/add_user <friend username>',
    '/card_balance': '/card_balance <card number>',
    '/account_balance': '/account_balance <account number>',


}
