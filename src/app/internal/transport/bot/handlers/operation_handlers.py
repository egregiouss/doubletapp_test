from telegram import Update
from telegram.ext import CallbackContext

from app.internal.services.balance_service import get_account_by_id, get_card_by_number
from app.internal.services.operation_record_service import (
    get_records_by_account,
    get_records_by_card,
    get_related_users,
    get_users_transactions,
)
from app.internal.transport.bot.bot_api import (
    parse_input,
    print_collection,
    send_no_related_msg,
    send_no_transactions_msg,
)
from app.internal.transport.bot.decorators import arguments_count, registered


@registered
def show_card_statement(update: Update, context: CallbackContext, user=None):
    card_number, end_date, start_date = parse_input(context)
    card_from = get_card_by_number(card_number)
    records_list = get_records_by_card(card_from, start_date, end_date)
    if len(records_list) != 0:
        print_collection(records_list, update)
    else:
        send_no_transactions_msg(context, update)


@registered
def show_account_statement(update: Update, context: CallbackContext, user=None):
    account_id, end_date, start_date = parse_input(context)
    acc_from = get_account_by_id(account_id)
    records_list = get_records_by_account(acc_from, start_date, end_date)
    if len(records_list) != 0:
        print_collection(records_list, update)
    else:
        send_no_transactions_msg(context, update)


@registered
def show_transaction_history(update: Update, context: CallbackContext, user=None):
    records_list = get_users_transactions(update, user)
    if len(records_list) != 0:
        story = "\n".join(
            [f"{i + 1}). {list(rec.values())[0]} -> {list(rec.values())[1]}" for i, rec in enumerate(records_list)])
        update.message.reply_text(story)
    else:
        send_no_transactions_msg(context, update)


@registered
def show_related_users(update: Update, context: CallbackContext, user=None):
    records_list = get_related_users(update, user)
    if len(records_list) != 0:
        user_set = set(j for i in records_list for j in i.values())
        print_collection(user_set, update)
    else:
        send_no_related_msg(context, update)
