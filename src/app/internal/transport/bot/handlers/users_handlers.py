from django.core.exceptions import ObjectDoesNotExist
from telegram import Update
from telegram.ext import CallbackContext

import app.internal.transport.bot.bot_api as api
from app.internal.services.user_service import add_friend_to_user, get_user_friends, remove_friend_from_user
from app.internal.transport.bot.decorators import registered


@registered
def add_friend(update: Update, context: CallbackContext, user):
    friend_username = context.args[0]
    try:
        add_friend_to_user(user, friend_username)
        update.message.reply_text(f"user {friend_username} added to your friendlist")
    except ObjectDoesNotExist:
        api.send_no_friend_msg(context, update, friend_username)


@registered
def remove_friend(update: Update, context: CallbackContext, user):
    friend_username = context.args[0]
    try:
        remove_friend_from_user(user, friend_username)
        update.message.reply_text(f"user {friend_username} removed from your friendlist")
    except ObjectDoesNotExist:
        api.send_no_friend_msg(context, update, friend_username)


@registered
def show_friends(update: Update, context: CallbackContext, user):
    friends_output = get_user_friends(user)
    update.message.reply_text(f"Friends:\n{friends_output}")
