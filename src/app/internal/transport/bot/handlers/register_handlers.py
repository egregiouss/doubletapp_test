from telegram import Update
from telegram.ext import CallbackContext

from app.internal.services.user_service import create_user, get_phone, update_phone
from app.internal.transport.bot.bot_api import send_incorrect_msg
from app.internal.transport.bot.decorators import registered


def start(update: Update, context: CallbackContext):
    id = update.effective_user.id
    first_name = update.message.from_user.first_name
    last_name = update.message.from_user.last_name
    username = update.message.from_user.username
    create_user(id, first_name, last_name, username)
    update.message.reply_text("Successfully added!")


@registered
def set_phone(update: Update, context: CallbackContext, user=None):
    user_input = context.args
    phone_number = get_phone(user_input)
    if not phone_number:
        send_incorrect_msg(context, update, '/set_phone')
        return
    update_phone(context, phone_number, update, user)
