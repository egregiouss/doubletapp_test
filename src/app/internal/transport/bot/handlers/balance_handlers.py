from telegram import Update
from telegram.ext import CallbackContext

from app.internal.services.balance_service import get_account_balance, get_card_balance
from app.internal.transport.bot.bot_api import send_balance_msg
from app.internal.transport.bot.decorators import account_exists, arguments_count, phone_set, registered


@arguments_count([1])
@registered
@phone_set
@account_exists
def card_balance(update: Update, context: CallbackContext, user=None):
    card_num = context.args[0]
    balance = get_card_balance(card_num)
    send_balance_msg(balance, context, update)


@arguments_count([1])
@registered
@phone_set
@account_exists
def account_balance(update: Update, context: CallbackContext, user=None):
    user_input = context.args
    account_id = user_input[0]
    balance = get_account_balance(account_id)
    send_balance_msg(balance, context, update)
