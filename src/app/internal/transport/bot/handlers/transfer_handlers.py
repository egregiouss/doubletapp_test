from telegram import Update
from telegram.ext import CallbackContext

from app.internal.services.custom_exceptions import BaseCustomException
from app.internal.services.transaction_service import transfer_by_username, transfer_to_account, transfer_to_card
from app.internal.services.user_service import get_user_by_username
from app.internal.transport.bot.bot_api import send_balance_msg, send_no_user_msg, send_not_in_friends_msg
from app.internal.transport.bot.decorators import account_exists, arguments_count, registered


@arguments_count([3])
@registered
@account_exists
def transfer_money_to_account(update: Update, context: CallbackContext, user=None):
    from_card, to_account, amount = context.args
    try:
        current_balance = transfer_to_account(user, int(from_card), int(to_account), int(amount))
        send_balance_msg(current_balance, context, update)
    except BaseCustomException as e:
        update.message.reply_text(str(e))


@arguments_count([3])
@registered
@account_exists
def transfer_money_to_card(update: Update, context: CallbackContext, user=None):
    from_card, to_card, amount = context.args
    try:
        current_balance = transfer_to_card(user, int(from_card), int(to_card), int(amount))
        send_balance_msg(current_balance, context, update)
    except BaseCustomException as e:
        update.message.reply_text(str(e))


@arguments_count([2])
@registered
@account_exists
def transfer_money_by_username(update: Update, context: CallbackContext, user=None):
    to_username, amount = context.args
    to_user = get_user_by_username(to_username)
    if to_user is None:
        send_no_user_msg(context, update)
    if to_user in user.friends.all():
        try:
            current_balance = transfer_by_username(user, to_user, int(amount))
            send_balance_msg(current_balance, context, update)
        except BaseCustomException as e:
            update.message.reply_text(str(e))
    else:
        send_not_in_friends_msg(context, update, to_user)
