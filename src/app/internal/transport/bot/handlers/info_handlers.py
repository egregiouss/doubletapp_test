from telegram import Update
from telegram.ext import CallbackContext

from app.internal.services.user_service import get_user_cards
from app.internal.transport.bot.decorators import phone_set, registered


@registered
@phone_set
def me(update: Update, context: CallbackContext, user=None):
    cards = "\n".join([f"{i + 1}). {card}" for i, card in enumerate(get_user_cards(user))])
    if len(cards) == 0:
        cards = "No cards"
    update.message.reply_text(f"Name: {user.first_name + ' ' + user.last_name}\n"
                              f"Phone number: {user.phone_number}\n"
                              f"Telegram username: {user.tg_username}\n"
                              f"Cards: \n{cards}\n")
