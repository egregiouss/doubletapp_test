from telegram import Update

from app.internal.transport.bot.errors import BotErrors


def send_no_user_msg(context, update: Update):
    update.message.reply_text(BotErrors.no_user_message())


def send_no_friend_msg(context, update, friend):
    context.bot.send_message(chat_id=update.effective_chat.id, text=BotErrors.no_friend_message(friend))


def send_no_phone_msg(context, update: Update):
    update.message.reply_text(BotErrors.no_phone_number())


def send_no_account_msg(context, update):
    context.bot.send_message(chat_id=update.effective_chat.id, text=BotErrors.no_account_message())


def send_incorrect_msg(context, update, key):
    update.message.reply_text(BotErrors.incorrect_format_message(key))


def send_no_card_msg(context, update):
    context.bot.send_message(chat_id=update.effective_chat.id, text=BotErrors.no_card_message())


def send_balance_msg(balance, context, update):
    update.message.reply_text(f"balance:{balance}")


def send_not_in_friends_msg(context, update, name):
    update.message.reply_text(f"{name} not in your friend list")


def send_no_transactions_msg(context, update):
    update.message.reply_text("No transactions")


def send_no_related_msg(context, update):
    update.message.reply_text("No related users")


def print_collection(records_list, update):
    story = "\n".join([f"{i + 1}). {rec}" for i, rec in enumerate(records_list)])
    update.message.reply_text(story)


def parse_input(context):
    account_id = context.args[0]
    start_date = context.args[1]
    end_date = context.args[2]
    return account_id, end_date, start_date


def print_transaction_history(records_list, update):
    story = "\n".join(
        [f"{i + 1}). {list(rec.values())[0]} -> {list(rec.values())[1]}" for i, rec in enumerate(records_list)])
    update.message.reply_text(story)
