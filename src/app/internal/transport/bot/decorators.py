from functools import wraps

from telegram import Update
from telegram.ext import CallbackContext

from app.internal.services.user_service import get_user_by_id
from app.internal.transport.bot.bot_api import (
    send_incorrect_msg,
    send_no_account_msg,
    send_no_phone_msg,
    send_no_user_msg,
)


def registered(handler):
    @wraps(handler)
    def wrapper(update: Update, context: CallbackContext, *args, **kwargs):
        user = get_user_by_id(update.effective_user.id)
        if user is None:
            send_no_user_msg(context, update)
            return
        return handler(update, context, user)
    return wrapper


def account_exists(handler):
    @wraps(handler)
    def wrapper(update: Update, context: CallbackContext, *args, **kwargs):
        user = get_user_by_id(update.effective_user.id)
        if not user.account_for_transfer:
            send_no_account_msg(context, update)
            return
        return handler(update, context, user)
    return wrapper


def phone_set(handler):
    @wraps(handler)
    def wrapper(update: Update, context: CallbackContext, *args, **kwargs):
        user = get_user_by_id(update.effective_user.id)
        if not user.phone_number:
            send_no_phone_msg(context, update)
            return
        return handler(update, context, user)
    return wrapper


def arguments_count(count_args: list):
    def inner(handler):
        @wraps(handler)
        def wrapper(update: Update, context: CallbackContext):
            if len(context.args) in count_args:
                handler(update, context, None)
                return
            else:
                key = update.message.text.split(" ")[0]
                send_incorrect_msg(context, update, key)
                return
        return wrapper
    return inner
