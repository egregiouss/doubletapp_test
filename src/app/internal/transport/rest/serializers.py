from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpRequest, JsonResponse
from django.views import View

from app.internal.services.user_service import get_user_by_id, get_user_by_username_token


class UserSerializer:
    def __init__(self, user):
        self.user = user

    def serialize(self):
        return JsonResponse(
            {
                "telegram_id": self.user.id,
                "username": self.user.tg_username,
                "phone_number": str(self.user.phone_number),
            },
            status=200
        )
