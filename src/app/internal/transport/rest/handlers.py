from typing import Union

from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpRequest, HttpResponseNotFound, JsonResponse
from django.views import View

from app.internal.bot import webhhok_bot
from app.internal.services.user_service import get_user_by_id, get_user_by_username_token
from app.internal.transport.rest.serializers import UserSerializer


def not_found_resp(token, name):
    return JsonResponse({"error": {"message": "user is not found"}}, status=404)


class UserInfoView(View):
    def get(self, request):
        username = request.GET.get("tg_name", None)
        token = request.GET.get("token", None)
        user = get_user_by_username_token(username, token, not_found_resp)
        if user is None:
            return not_found_resp(token, username)
        if user is not None and not user.phone_number:
            return JsonResponse({"error": {"field": "phone_number", "message": "Phone number not set"}})
        serializer = UserSerializer(user)

        return serializer.serialize()


class TelegramBotView(View):
    def post(self, request: HttpRequest) -> Union[JsonResponse, HttpResponseNotFound]:
        webhhok_bot.update(request.body)
        return JsonResponse(data={"OK": "POST request processed"})

    def get(self, request: HttpRequest) -> Union[JsonResponse, HttpResponseNotFound]:

        return JsonResponse(data={"OK": "works"})
