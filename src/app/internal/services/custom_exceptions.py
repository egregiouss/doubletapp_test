class BaseCustomException(Exception):
    pass


class NotEnoughMoneyError(BaseCustomException):
    def __init__(self, balance):
        self.arg = balance

    def __str__(self):
        return f"Not Enough Money, balance: {self.arg}"


class SameAccountError(BaseCustomException):
    def __init__(self, account_num):
        self.arg = account_num

    def __str__(self):
        return f"You cant move money to same account number {self.arg}"


class AccountDoesNotExistsError(BaseCustomException):
    def __init__(self, account_num):
        self.arg = account_num

    def __str__(self):
        return f"No account number {self.arg}"


class CardDoesNotExistsError(BaseCustomException):
    def __init__(self, card_num):
        self.arg = card_num

    def __str__(self):
        return f"No card number {self.arg}"


class AccessDenied(BaseCustomException):
    def __init__(self):
        pass

    def __str__(self):
        return "Its not your card to transfer money from it !!!"


class UserNotFriendException(BaseCustomException):
    def __init__(self, name):
        self.name = name

    def __str__(self):
        return f"{ self.name} not in your friend list"
