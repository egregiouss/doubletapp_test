from django.core.exceptions import ObjectDoesNotExist

from app.internal.models.bank_account import BankAccount
from app.internal.models.card import Card


def get_card_balance(card_num: str) -> int:
    balance = Card.objects.filter(card_number=card_num).values_list('account__balance', flat=True).first()
    if balance is None:
        raise ObjectDoesNotExist
    return balance


def get_account_balance(account_id: str) -> int:
    balance = BankAccount.objects.get(id=account_id).balance
    if balance is None:
        raise ObjectDoesNotExist
    return balance


def get_account_by_id(account_id: str, *args):
    try:
        account = BankAccount.objects.select_related().get(id=account_id)
        return account
    except ObjectDoesNotExist:
        return None


def get_card_by_number(number, *args):
    try:
        card = Card.objects.select_related().get(card_number=number)
        return card
    except ObjectDoesNotExist:
        return None


def get_card_account_by_number(number, *args):
    try:
        account = Card.objects.select_related().get(card_number=number).account
        return account
    except ObjectDoesNotExist:
        return None
