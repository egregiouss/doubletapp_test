import re
import secrets
import string

from django.core.exceptions import ObjectDoesNotExist

from app.internal.models.card import Card
from app.internal.models.user import User

PHONE_PATTERN = re.compile(r"^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$")


def get_user_by_id(id):
    try:
        user = User.objects.get(id=id)
        return user
    except ObjectDoesNotExist:
        return None


def get_user_by_username(username):
    try:
        user = User.objects.get(tg_username=username)
        return user
    except ObjectDoesNotExist:
        return None


def get_user_cards(user, *args):
    try:
        cards = Card.objects.filter(account__id=user.account_for_transfer.id).all()
        return cards
    except ObjectDoesNotExist:
        return None


def get_user_by_username_token(username, token, func_if_exc, *args):
    try:
        user = User.objects.get(tg_username=username, token=token)
        return user
    except ObjectDoesNotExist:
        return None


def update_phone(context, phone_number, update, user):
    msg = (
        "phone number was changed successfully"
        if user.phone_number is not None
        else "Successfully " "added phone " "number "
    )
    user.phone_number = phone_number
    user.save()
    update.message.reply_text(msg)


def get_phone(text):
    if not text:
        return None
    text = text[0]
    match = re.search(PHONE_PATTERN, text)
    return match.group() if match is not None else None


def generate_token(length):
    letters_and_digits = string.ascii_letters + string.digits
    crypt_rand_string = "".join(secrets.choice(letters_and_digits) for i in range(length))
    return crypt_rand_string


def create_user(id, first_name, last_name, username):
    token = generate_token(16)
    User.objects.get_or_create(
        id=id, defaults={"first_name": first_name, "last_name": last_name, "tg_username": username, "token": token}
    )


def add_friend_to_user(user, friend_username):
    friend = get_user_by_username(friend_username)
    if friend is None:
        raise ObjectDoesNotExist
    user.friends.add(friend)


def remove_friend_from_user(user, friend_username):
    friend = get_user_by_username(friend_username)
    if friend is None:
        raise ObjectDoesNotExist
    user.friends.remove(friend)


def get_user_friends(user):
    friend_list = list(user.friends.all())
    if len(friend_list) == 0:
        return "no friends"
    return "\n".join([f"{i + 1} {friend}" for i, friend in enumerate(friend_list)])
