from django.db import transaction
from django.db.models import F

from app.internal.models.bank_account import BankAccount
from app.internal.services.balance_service import get_account_by_id, get_card_account_by_number, get_card_by_number
from app.internal.services.custom_exceptions import (
    AccessDenied,
    AccountDoesNotExistsError,
    CardDoesNotExistsError,
    NotEnoughMoneyError,
    SameAccountError,
)
from app.internal.services.operation_record_service import create_account_record, create_card_record


def transfer_to_card(user, from_card_num, to_card_num, amount):
    from_card = get_card_by_number(from_card_num)
    if from_card is None:
        raise CardDoesNotExistsError(from_card_num)
    if from_card.account.owner.id != user.id:
        raise AccessDenied()
    to_card = get_card_by_number(to_card_num)
    if to_card is None:
        raise CardDoesNotExistsError(to_card_num)
    move_money(user, from_card.account, to_card.account, amount)
    create_card_record(user, to_card.account.owner, from_card, to_card, amount)
    return from_card.account.balance


def transfer_to_account(user, from_card_num, to_account_num, amount):
    from_acc = get_card_account_by_number(from_card_num)
    if from_acc is None:
        raise CardDoesNotExistsError(from_card_num)
    if from_acc.owner.id != user.id:
        raise AccessDenied()
    to_account = get_account_by_id(to_account_num)
    if to_account is None:
        raise AccountDoesNotExistsError(to_account_num)
    move_money(user, from_acc, to_account, amount)
    create_account_record(user, to_account.owner, from_acc, to_account, amount)
    return from_acc.balance


def transfer_by_username(user, to_user, amount):
    from_account = user.account_for_transfer
    move_money(user, from_account, to_user.account_for_transfer, amount)
    create_account_record(user, to_user, from_account, to_user.account_for_transfer, amount)
    return from_account.balance


def move_money(user, account_from: BankAccount, account_to: BankAccount, amount: int):
    if account_from.balance < amount:
        raise NotEnoughMoneyError(account_from.balance)
    if account_to.id == account_from.id:
        raise SameAccountError(account_to.id)
    account_from.balance = F("balance") - amount
    account_to.balance = F("balance") + amount
    account_from.save()
    account_to.save()
    account_from.refresh_from_db()
    account_to.refresh_from_db()
