import datetime

from django.db.models import Q
from telegram import Update

from app.internal.models.account_record import AccountRecord
from app.internal.models.bank_account import BankAccount
from app.internal.models.card import Card
from app.internal.models.card_record import CardRecord
from app.internal.models.operation_record import OperationRecord
from app.internal.models.user import User


def create_card_record(from_user, to_user, cardFrom, cardTo,
                       amount: int):
    CardRecord.objects.create(from_user=from_user,
                              to_user=to_user,
                              card_from=cardFrom,
                              card_to=cardTo,
                              amount=amount)


def create_account_record(from_user, to_user, accFrom, accTo,
                          amount: int):
    AccountRecord.objects.create(from_user=from_user,
                                 to_user=to_user,
                                 acc_from=accFrom,
                                 acc_to=accTo,
                                 amount=amount)


def get_records_by_card(card: Card, start_date, end_date) -> list:
    start_date_obj = parse_date_args(start_date)
    end_date_obj = parse_date_args(end_date)
    return CardRecord.objects.filter((
        Q(card_from=card) & Q(created_at__gte=start_date_obj) & Q(created_at__lte=end_date_obj))
        | (Q(card_to=card) & Q(created_at__gte=start_date_obj) & Q(created_at__lte=end_date_obj))).all()


def get_records_by_account(acc: BankAccount, start_date, end_date) -> list:
    start_date_obj = parse_date_args(start_date)
    end_date_obj = parse_date_args(end_date)
    return AccountRecord.objects.select_related().filter((
        Q(acc_from=acc) & Q(created_at__gte=start_date_obj) & Q(created_at__lte=end_date_obj))
        | (Q(acc_to=acc) & Q(created_at__gte=start_date_obj) & Q(created_at__lte=end_date_obj))).all()


def get_all_records_by_account(userFrom: User):
    return AccountRecord.objects.select_related().filter(from_user=userFrom).all()


def get_all_records_by_card(userFrom: User):
    return CardRecord.objects.select_related().filter(from_user=userFrom).all()


def get_related_users(update: Update, user):
    return AccountRecord.objects.select_related().filter(Q(from_user=user) | Q(to_user=user)) \
        .values('from_user__tg_username', 'to_user__tg_username') \
        .distinct()


def get_users_transactions(update: Update, user):
    return AccountRecord.objects.select_related().filter(Q(from_user=user) | Q(to_user=user)) \
        .values('from_user__tg_username', 'to_user__tg_username')


def parse_date_args(date_str):
    return datetime.datetime.strptime(date_str, '%d-%m-%Y')
