from django.core.validators import RegexValidator
from django.db import models

expired_validator = RegexValidator(
    regex=r"(\d{2}\/\d{2})",
    message="Incorrect expiring date",
)


class Card(models.Model):
    card_number = models.CharField(max_length=50, verbose_name="Card number", unique=True, primary_key=True, null=False)
    account = models.ForeignKey("BankAccount", on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.card_number} | Account: {self.account.id} | Balance:{self.account.balance}"
