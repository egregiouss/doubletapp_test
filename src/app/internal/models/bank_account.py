from decimal import Decimal

from django.core.validators import MinValueValidator
from django.db import models


class BankAccount(models.Model):
    balance = models.DecimalField(decimal_places=2,
                                  max_digits=12,
                                  validators=[MinValueValidator(Decimal('0.01'))],
                                  verbose_name="Balance",
                                  null=False,
                                  default=0)
    owner = models.ForeignKey(
        "User",
        on_delete=models.CASCADE
    )
