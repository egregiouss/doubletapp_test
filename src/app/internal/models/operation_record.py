from decimal import Decimal

from django.core.validators import MinValueValidator, RegexValidator
from django.db import models


class OperationRecord(models.Model):
    from_user = models.ForeignKey("User",
                                  on_delete=models.DO_NOTHING,
                                  related_name="%(app_label)s_%(class)s_from_user",
                                  related_query_name="%(app_label)s_%(class)ss")
    to_user = models.ForeignKey("User", on_delete=models.DO_NOTHING, related_name="%(app_label)s_%(class)s_to_user")

    amount = models.DecimalField(decimal_places=2,
                                 max_digits=12,
                                 validators=[MinValueValidator(Decimal('0.01'))],
                                 verbose_name="amount",
                                 null=False,
                                 default=0)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"Transferred {self.amount} to {self.to_user} at {self.created_at}"

    class Meta:
        abstract = True
