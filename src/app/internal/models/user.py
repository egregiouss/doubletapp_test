from django.core.validators import RegexValidator
from django.db import models


class User(models.Model):
    phone_validator = RegexValidator(
        regex=r"^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$",
        message="Incorrect phone number format",
    )

    id = models.IntegerField(primary_key=True, verbose_name="Id")

    first_name = models.CharField(max_length=50, verbose_name="User's first name")
    last_name = models.CharField(max_length=50, verbose_name="User's last name", blank=True, null=True)
    tg_username = models.CharField(max_length=50, verbose_name="Telegram nickname", unique=True)
    phone_number = models.CharField(
        validators=[phone_validator], max_length=12, default=None, blank=True, null=True, verbose_name="Phone number"
    )
    token = models.CharField(max_length=50, verbose_name="Auth token", unique=True)
    friends = models.ManyToManyField("User", blank=True)
    account_for_transfer = models.OneToOneField("BankAccount", on_delete=models.SET_NULL, blank=True, null=True)

    def __str__(self):
        return f"{self.tg_username}"

    class Meta:
        verbose_name = "User"
        verbose_name_plural = "Users"
