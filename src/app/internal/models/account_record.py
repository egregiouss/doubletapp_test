from decimal import Decimal

from django.core.validators import MinValueValidator, RegexValidator
from django.db import models

from app.internal.models.operation_record import OperationRecord


class AccountRecord(OperationRecord):
    acc_from = models.ForeignKey("BankAccount", on_delete=models.DO_NOTHING, related_name="acc_from")
    acc_to = models.ForeignKey("BankAccount", on_delete=models.DO_NOTHING, related_name="acc_to")

    def __str__(self):
        return f"Transferred {self.amount} from account {self.acc_from.id} to {self.acc_to.id} at" \
               f" {self.created_at.strftime('%d.%m.%Y - %H:%M')}"
