from django.contrib.auth.models import AbstractUser


class AdminUser(AbstractUser):
    class Meta:
        verbose_name = "Administrator"
        verbose_name_plural = "Administrators"
