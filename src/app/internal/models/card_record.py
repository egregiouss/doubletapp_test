from decimal import Decimal

from django.core.validators import MinValueValidator, RegexValidator
from django.db import models

from app.internal.models.operation_record import OperationRecord


class CardRecord(OperationRecord):
    card_from = models.ForeignKey("Card", on_delete=models.DO_NOTHING, related_name="acc_from")
    card_to = models.ForeignKey("Card", on_delete=models.DO_NOTHING, related_name="acc_to")

    def __str__(self):
        return f"Transferred {self.amount} from account {self.card_from.card_number} to {self.card_to.card_number} at" \
               f" {self.created_at.strftime('%d.%m.%Y - %H:%M')}"
