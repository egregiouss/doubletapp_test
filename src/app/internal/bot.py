import json

import telegram
from telegram.ext import CommandHandler, Dispatcher, Updater

from app.internal.transport.bot.handlers.balance_handlers import account_balance, card_balance
from app.internal.transport.bot.handlers.info_handlers import me
from app.internal.transport.bot.handlers.operation_handlers import (
    show_account_statement,
    show_card_statement,
    show_related_users,
    show_transaction_history,
)
from app.internal.transport.bot.handlers.register_handlers import set_phone, start
from app.internal.transport.bot.handlers.transfer_handlers import (
    transfer_money_by_username,
    transfer_money_to_account,
    transfer_money_to_card,
)
from app.internal.transport.bot.handlers.users_handlers import add_friend, remove_friend, show_friends
from config.settings import BOT_PORT, BOT_TOKEN, DOMAIN_NAME


class TelegramBot:
    def __init__(self, token):
        self.bot = telegram.Bot(token=token)
        self.dispatcher = Dispatcher(self.bot, None, workers=0)
        self.add_handlers()

    def update(self, text: str):
        update = telegram.Update.de_json(json.loads(text), self.bot)
        self.dispatcher.process_update(update)

    def add_handlers(self):
        dispatcher = self.dispatcher
        dispatcher.add_handler(CommandHandler("start", start))
        dispatcher.add_handler(CommandHandler("set_phone", set_phone))
        dispatcher.add_handler(CommandHandler("me", me))
        dispatcher.add_handler(CommandHandler("card_balance", card_balance))
        dispatcher.add_handler(CommandHandler("account_balance", account_balance))
        dispatcher.add_handler(CommandHandler("add_friend", add_friend))
        dispatcher.add_handler(CommandHandler("remove_friend", remove_friend))
        dispatcher.add_handler(CommandHandler("show_friends", show_friends))
        dispatcher.add_handler(CommandHandler("transfer_money_to_account", transfer_money_to_account))
        dispatcher.add_handler(CommandHandler("transfer_money_to_card", transfer_money_to_card))
        dispatcher.add_handler(CommandHandler("transfer_money_by_username", transfer_money_by_username))
        dispatcher.add_handler(CommandHandler("show_card_statement", show_card_statement))
        dispatcher.add_handler(CommandHandler("show_account_statement", show_account_statement))
        dispatcher.add_handler(CommandHandler("show_transaction_history", show_transaction_history))
        dispatcher.add_handler(CommandHandler("show_related_users", show_related_users))


webhhok_bot = TelegramBot(token=BOT_TOKEN)
