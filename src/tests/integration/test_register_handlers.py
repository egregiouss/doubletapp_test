import pytest

from app.internal.services import user_service
from app.internal.transport.bot.errors import BotErrors
from app.internal.transport.bot.handlers.register_handlers import set_phone, start


@pytest.mark.django_db
@pytest.mark.integration
def test_start(update_mock, context_mock):
    start(update_mock, context_mock)
    assert user_service.get_user_by_username("test_username") is not None
    update_mock.message.reply_text.assert_called_once_with("Successfully added!")


@pytest.mark.django_db
@pytest.mark.integration
def test_set_phone(update_mock, context_mock, first_user):
    context_mock.args = ["+71234567788"]
    set_phone(update_mock, context_mock, first_user)
    update_mock.message.reply_text.assert_called_once_with("Successfully added phone number ")


@pytest.mark.django_db
@pytest.mark.integration
def test_set_phone_incorrect(update_mock, context_mock, first_user):
    context_mock.args = []
    set_phone(update_mock, context_mock, first_user)
    update_mock.message.reply_text.assert_called_once_with(BotErrors.incorrect_format_message('/set_phone'))
