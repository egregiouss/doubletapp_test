from unittest.mock import MagicMock

import pytest


@pytest.fixture(scope="function")
def update_mock():
    mocked_update = MagicMock()
    mocked_message = MagicMock()
    mocked_effective_user = MagicMock()
    mocked_effective_user.id = 1

    mocked_message.reply_text = MagicMock()
    mocked_message.from_user = MagicMock(first_name="test_name",
                                         last_name="test_last_name",
                                         username="test_username")

    mocked_update.message = mocked_message
    mocked_update.effective_user = mocked_effective_user

    return mocked_update


@pytest.fixture(scope="function")
def context_mock():
    mock = MagicMock()
    return mock
