import pytest

from app.internal.transport.bot.errors import BotErrors
from app.internal.transport.bot.handlers.info_handlers import me
from tests.unit.utils import prepare_user


@pytest.mark.django_db
@pytest.mark.integration
def test_me_no_cards(update_mock, context_mock, first_user, first_account):
    prepare_user(first_account, first_user)
    me(update_mock, context_mock, first_user)
    update_mock.message.reply_text.assert_called_once_with(
        f"Name: {first_user.first_name + ' ' + first_user.last_name}\n"
        f"Phone number: {first_user.phone_number}\n"
        f"Telegram username: {first_user.tg_username}\n"
        f"Cards: \nNo cards\n")


@pytest.mark.django_db
@pytest.mark.integration
def test_me(update_mock, context_mock, first_user, first_account, first_card):
    prepare_user(first_account, first_user)
    me(update_mock, context_mock, first_user)
    update_mock.message.reply_text.assert_called_once_with(
        f"Name: {first_user.first_name + ' ' + first_user.last_name}\n"
        f"Phone number: {first_user.phone_number}\n"
        f"Telegram username: {first_user.tg_username}\n"
        f"Cards: \n1). 1 | Account: 1 | Balance:100.00\n")


@pytest.mark.django_db
@pytest.mark.integration
def test_me_no_phone(update_mock, context_mock, first_user, first_account, first_card):
    me(update_mock, context_mock, first_user)
    update_mock.message.reply_text.assert_called_once_with(BotErrors.no_phone_number())


@pytest.mark.django_db
@pytest.mark.integration
def test_me_no_user(update_mock, context_mock):
    me(update_mock, context_mock)
    update_mock.message.reply_text.assert_called_once_with(BotErrors.no_user_message())
