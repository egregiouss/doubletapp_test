import pytest

from app.internal.services import user_service
from app.internal.transport.bot.handlers.transfer_handlers import (
    transfer_money_by_username,
    transfer_money_to_account,
    transfer_money_to_card,
)
from tests.unit.utils import prepare_user


@pytest.mark.parametrize("amount,expected", [("10", "balance:90.00"), ("1000", "Not Enough Money, balance: 100.00")])
@pytest.mark.django_db
@pytest.mark.integration
def test_transfer_money_to_account(update_mock,
                                   context_mock,
                                   first_user,
                                   first_card,
                                   first_account,
                                   second_account,
                                   amount,
                                   expected):
    prepare_user(first_account, first_user)
    context_mock.args = [first_card.card_number, second_account.id, amount]
    transfer_money_to_account(update_mock, context_mock)
    update_mock.message.reply_text.assert_called_once_with(expected)


@pytest.mark.parametrize("amount,expected",
                         [("10", "balance:90.00"),
                          ("1000", "Not Enough Money, balance: 100.00")])
@pytest.mark.django_db
@pytest.mark.integration
def test_transfer_money_to_card(update_mock,
                                context_mock,
                                first_user,
                                first_card,
                                second_card,
                                first_account,
                                second_account,
                                amount,
                                expected):
    prepare_user(first_account, first_user)
    context_mock.args = [first_card.card_number, second_card.card_number, amount]
    transfer_money_to_card(update_mock, context_mock)
    update_mock.message.reply_text.assert_called_once_with(expected)


@pytest.mark.parametrize("amount, is_friend_exist, expected",
                         [("10", True, "balance:90.00"),
                          ("1000", True, "Not Enough Money, balance: 100.00"),
                          ("20", False, "username2 not in your friend list")])
@pytest.mark.django_db
@pytest.mark.integration
def test_transfer_money_by_username(update_mock,
                                    context_mock,
                                    first_user,
                                    first_card,
                                    second_card,
                                    second_user,
                                    first_account,
                                    second_account,
                                    amount,
                                    is_friend_exist,
                                    expected):
    prepare_user(first_account, first_user)
    prepare_user(second_account, second_user)
    if is_friend_exist:
        user_service.add_friend_to_user(first_user, second_user.tg_username)
    context_mock.args = [second_user, amount]
    transfer_money_by_username(update_mock, context_mock)
    update_mock.message.reply_text.assert_called_once_with(expected)
