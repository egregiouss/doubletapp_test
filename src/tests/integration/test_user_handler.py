import pytest

from app.internal.services import user_service
from app.internal.transport.bot.handlers.users_handlers import add_friend, remove_friend, show_friends


@pytest.mark.django_db
@pytest.mark.integration
def test_add_friend(update_mock, context_mock, first_user, second_user):
    context_mock.args = [second_user.tg_username]
    add_friend(update_mock, context_mock, first_user)
    update_mock.message.reply_text.assert_called_once_with(f"user {second_user.tg_username} added to your friendlist")


@pytest.mark.django_db
@pytest.mark.integration
def test_remove_friend(update_mock, context_mock, first_user, second_user):
    context_mock.args = [second_user.tg_username]
    add_friend(update_mock, context_mock, first_user)
    update_mock.message.reply_text.assert_called_once_with(f"user {second_user.tg_username} added to your friendlist")
    remove_friend(update_mock, context_mock, first_user)
    update_mock.message.reply_text.assert_called_with(f"user {second_user.tg_username} removed from your friendlist")


@pytest.mark.django_db
@pytest.mark.integration
def test_show_friends(update_mock, context_mock, first_user, second_user):
    user_service.add_friend_to_user(first_user, second_user)
    show_friends(update_mock, context_mock, first_user)
    update_mock.message.reply_text.assert_called_once_with("Friends:\n1 username2")
