import pytest

from app.internal.transport.bot.handlers.balance_handlers import account_balance, card_balance
from tests.unit.utils import prepare_user


@pytest.mark.django_db
@pytest.mark.integration
def test_card_balance(update_mock, context_mock, first_card, first_account, first_user):
    prepare_user(first_account, first_user)
    context_mock.args = [1]
    card_balance(update_mock, context_mock)
    update_mock.message.reply_text.assert_called_once_with("balance:100.00")


@pytest.mark.django_db
@pytest.mark.integration
def test_account_balance(update_mock, context_mock, first_card, first_account, first_user):
    prepare_user(first_account, first_user)
    context_mock.args = [1]
    account_balance(update_mock, context_mock)
    update_mock.message.reply_text.assert_called_once_with("balance:100.00")
