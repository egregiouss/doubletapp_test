import pytest
from django.test import Client

from app.internal.models.bank_account import BankAccount
from app.internal.models.card import Card
from app.internal.models.user import User


@pytest.fixture(scope="function")
@pytest.mark.django_db
def first_user(first_name="A", last_name="A", username="username1"):
    return User.objects.create(id=1, first_name=first_name, last_name=last_name, tg_username=username, token=1)


@pytest.fixture(scope="function")
@pytest.mark.django_db
def second_user(first_name="B", last_name="B", username="username2"):
    return User.objects.create(id=2, first_name=first_name, last_name=last_name, tg_username=username, token=2)


@pytest.fixture(scope="function")
@pytest.mark.django_db
def third_user(first_name="C", last_name="C", username="username3"):
    return User.objects.create(id=3, first_name=first_name, last_name=last_name, tg_username=username, token=3)


@pytest.fixture(scope="function")
@pytest.mark.django_db
def first_account(first_user, balance=100):
    return BankAccount.objects.create(id=1, owner=first_user, balance=balance)


@pytest.fixture(scope="function")
@pytest.mark.django_db
def second_account(second_user, balance=50):
    return BankAccount.objects.create(id=2, owner=second_user, balance=balance)


@pytest.fixture(scope="function")
@pytest.mark.django_db
def first_card(first_account, card_number=1):
    return Card.objects.create(card_number=card_number, account=first_account)


@pytest.fixture(scope="function")
@pytest.mark.django_db
def second_card(second_account, card_number=2):
    return Card.objects.create(card_number=card_number, account=second_account)
