import pytest

from app.internal.models.user import User
from app.internal.services.user_service import create_user


@pytest.mark.django_db
def test_user_create():
    create_user(1, "A", "A", "username")
    assert User.objects.count() == 1
