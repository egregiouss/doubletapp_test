import pytest
from django.core.exceptions import ObjectDoesNotExist

from app.internal.services import balance_service


@pytest.mark.unit
@pytest.mark.django_db
def test_get_card_balance(first_card):
    actual = balance_service.get_card_balance(first_card.card_number)
    expected = 100
    assert actual == expected


@pytest.mark.unit
@pytest.mark.django_db
def test_get_account_balance(second_account):
    actual = balance_service.get_account_balance(second_account.id)
    expected = 50
    assert actual == expected


@pytest.mark.unit
@pytest.mark.django_db
def test_get_card_balance_card_not_exists():
    with pytest.raises(ObjectDoesNotExist):
        balance_service.get_card_balance("123123")


@pytest.mark.unit
@pytest.mark.django_db
def test_get_account_balance_account_not_exists():
    with pytest.raises(ObjectDoesNotExist):
        balance_service.get_account_balance("123123")
