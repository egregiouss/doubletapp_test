import pytest
from django.core.exceptions import ObjectDoesNotExist

from app.internal.services import user_service
from tests.unit.utils import get_not_existed_user


@pytest.mark.unit
@pytest.mark.django_db
def test_get_user_by_username(first_user):
    actual = user_service.get_user_by_username(first_user.tg_username)
    expected = first_user
    assert actual.id == expected.id


@pytest.mark.unit
@pytest.mark.django_db
def test_get_user_by_id(first_user):
    actual = user_service.get_user_by_id(first_user.id)
    expected = first_user
    assert actual.id == expected.id


@pytest.mark.unit
@pytest.mark.django_db
def test_add_friend_to_user(first_user, second_user):
    user_service.add_friend_to_user(first_user, second_user)
    actual = list(user_service.get_user_by_id(first_user.id).friends.all())
    expected = [second_user]
    assert actual == expected


@pytest.mark.unit
@pytest.mark.django_db
def test_add_not_existed_friend_to_user(first_user, second_user):
    with pytest.raises(ObjectDoesNotExist):
        user_friend = get_not_existed_user()
        user_service.add_friend_to_user(first_user, user_friend)


@pytest.mark.unit
@pytest.mark.django_db
def test_remove_not_existed_friend_from_user(first_user, second_user):
    with pytest.raises(ObjectDoesNotExist):
        user_friend = get_not_existed_user()
        user_service.remove_friend_from_user(first_user, user_friend)


@pytest.mark.unit
@pytest.mark.django_db
def test_remove_friend_from_user(first_user, second_user):
    user_service.add_friend_to_user(first_user, second_user)
    user_service.remove_friend_from_user(first_user, second_user)
    actual = list(user_service.get_user_by_id(first_user.id).friends.all())
    expected = []
    assert actual == expected


@pytest.mark.unit
@pytest.mark.django_db
def test_show_friends(first_user, second_user, third_user):
    user_service.add_friend_to_user(first_user, second_user)
    user_service.add_friend_to_user(first_user, third_user)
    friends_output_actual = user_service.get_user_friends(first_user)
    friends_output_expected = "1 username2\n2 username3"
    assert friends_output_actual == friends_output_expected


@pytest.mark.unit
@pytest.mark.django_db
def test_show_friends_empty(first_user, second_user, third_user):
    friends_output_actual = user_service.get_user_friends(first_user)
    friends_output_expected = "no friends"
    assert friends_output_actual == friends_output_expected
