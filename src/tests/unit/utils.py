from app.internal.models.bank_account import BankAccount
from app.internal.models.card import Card
from app.internal.models.user import User


def set_acc_for_transfer(account, first_user):
    first_user.account_for_transfer = account


def get_not_existed_user():
    user_friend = User()
    user_friend.tg_username = "asd"
    return user_friend


def get_not_existed_card():
    card = Card()
    card.card_number = "123123"

    return card


def get_not_existed_account():
    acc = BankAccount()
    acc.id = "123123"
    return acc


def prepare_user(account, user):
    user.phone_number = '+71234567890'
    user.account_for_transfer = account
    user.save()


def change_username(user, new_name):
    user.tg_username = new_name
    user.save()
