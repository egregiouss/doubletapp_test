from decimal import Decimal

import pytest

from app.internal.services import balance_service, transaction_service
from app.internal.services.custom_exceptions import AccountDoesNotExistsError, CardDoesNotExistsError
from tests.unit.utils import get_not_existed_account, get_not_existed_card, set_acc_for_transfer


@pytest.mark.unit
@pytest.mark.django_db
def test_to_card_transfer(first_user, first_card, second_card, second_user):
    actual_balance_first = transaction_service.transfer_to_card(
        first_user,
        first_card.card_number,
        second_card.card_number,
        30)

    actual_balance_second = balance_service.get_card_balance(second_card.card_number)

    assert actual_balance_first == 70
    assert actual_balance_second == 80


@pytest.mark.unit
@pytest.mark.django_db
def test_to_card_not_exists_transfer(first_user, first_card, second_user):
    second_card = get_not_existed_card()
    with pytest.raises(CardDoesNotExistsError):
        transaction_service.transfer_to_card(
            first_user,
            first_card.card_number,
            second_card.card_number,
            30)


@pytest.mark.unit
@pytest.mark.django_db
def test_to_account_not_exists_transfer(first_user, first_card, second_user):
    second_account = get_not_existed_account()
    with pytest.raises(AccountDoesNotExistsError):
        transaction_service.transfer_to_account(
            first_user,
            first_card.card_number,
            second_account.id,
            30)


@pytest.mark.unit
@pytest.mark.django_db
def test_to_account_transfer(first_user, first_card, second_account, second_user):
    actual_balance_first = transaction_service.transfer_to_account(
        first_user,
        first_card.card_number,
        second_account.id,
        30)
    actual_balance_second = balance_service.get_account_balance(second_account.id)
    assert actual_balance_first == 70
    assert actual_balance_second == 80


@pytest.mark.unit
@pytest.mark.django_db
def test_to_user_transfer(first_user, first_card, second_user, first_account, second_account):
    set_acc_for_transfer(first_account, first_user)
    set_acc_for_transfer(second_account, second_user)
    actual_balance_first = transaction_service.transfer_by_username(first_user, second_user, 30)
    actual_balance_second = balance_service.get_account_balance(second_account.id)
    assert actual_balance_first == 70
    assert actual_balance_second == 80
