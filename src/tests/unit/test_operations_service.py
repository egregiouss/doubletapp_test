import pytest

from app.internal.models.account_record import AccountRecord
from app.internal.models.card_record import CardRecord
from app.internal.services import transaction_service
from tests.unit.utils import set_acc_for_transfer


@pytest.mark.django_db
def test_transfer_to_card_creates_record(first_user, first_card, second_card, second_user):
    transaction_service.transfer_to_card(
        first_user,
        first_card.card_number,
        second_card.card_number,
        30)
    assert CardRecord.objects.count() == 1


@pytest.mark.django_db
def test_transfer_to_acc_creates_record(first_user, first_card, second_account, second_user):
    transaction_service.transfer_to_account(
        first_user,
        first_card.card_number,
        second_account.id,
        20)
    assert AccountRecord.objects.count() == 1


@pytest.mark.django_db
def test_transfer_to_user_creates_record(first_user, first_card, first_account, second_account, second_user):
    set_acc_for_transfer(first_account, first_user)
    set_acc_for_transfer(second_account, second_user)
    transaction_service.transfer_by_username(
        first_user,
        second_user,
        20)

    assert AccountRecord.objects.count() == 1
