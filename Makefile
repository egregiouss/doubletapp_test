migrate:
	docker-compose run --rm web bash -c "python src/manage.py migrate $(if $m, api $m,)"

makemigrations:
	docker-compose run web python src/manage.py makemigrations && sudo chown -R ${USER} src/app/migrations/

createsuperuser:
	docker-compose exec web python3 src/manage.py createsuperuser

collectstatic:
	docker-compose run web python src/manage.py collectstatic --no-input

dev:
	docker-compose up -d --build
test:
	docker-compose run --rm web bash -c "cd src && pipenv run pytest"

build:
	docker-compose build

down:
	docker-compose down --remove-orphans

up:
	docker-compose up -d

command:
	docker-compose exec web python3 src/manage.py ${c}

shell:
	docker-compose exec web python3 src/manage.py shell

lint:
	docker-compose up -d --remove-orphans
	docker-compose run web isort --check --diff .
	docker-compose run web flake8 --config setup.cfg
	docker-compose down


debug:
	docker-compose exec web python3 src/manage.py debug

piplock:
	docker-compose exec web pipenv install
	docker-compose exec web chown -R ${USER} Pipfile.lock
